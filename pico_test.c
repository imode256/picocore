#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <SDL.h>
#include <pico.h>

pico_ucell start;
pico_ucell frames;

pico_bool update(pico_core *core) {
    pico_ucell iterator;
    for(iterator = 0; iterator < pico_actor_count; iterator++) {
        if(core->actors[iterator].update != pico_null) {
            core->actors[iterator].update(core, iterator);
            if(core->mailboxes[0][0].free == pico_false &&
               core->mailboxes[0][0].type == -1) {
                return pico_false;
            }
        }
    }
    return pico_true;
}

typedef struct c_state c_state;
struct c_state {
    pico_byte data[16][16];
};

typedef struct f_state f_state;
struct f_state {
    SDL_Window   *window;
    SDL_Renderer *renderer;
    SDL_Texture  *tileset;
    pico_ucell    old_time;
    pico_ucell    new_time;
    pico_ucell    delta_time;
    pico_ucell    destination;
    pico_ucell    type;
    pico_ucell    mouse_x, mouse_y;
};

typedef struct obj_state obj_state;
struct obj_state {
    pico_ucell x;
    pico_ucell y;
    pico_ucell type;
    pico_ucell delta_time;
    pico_ucell move_timer;
    pico_ucell animation_timer;
};

typedef struct f_message f_message;
struct f_message {
    pico_ucell delta_time;
    pico_bool up, down, left, right;
};

typedef struct obj_message obj_message;
struct obj_message {
    pico_ucell x, y, type;
};

void frontend(pico_core  *core,
              pico_ucell  self) {
    const Uint8        *keyboard;
          SDL_Surface  *surface;
          c_state      *world;
          f_state      *state;
          obj_message  *object;
          f_message    *input;
          pico_message  message;
          SDL_Rect      source;
          SDL_Rect      destination;
          SDL_Event     event;
          pico_ucell    iterator;
          pico_ucell    x, y;
    world          = (c_state*)core->data;
    state          = (f_state*)core->actors[self].data;
    message.sender = self;
    source.y       = 0;
    source.w       = 8;
    source.h       = 8;
    destination.w  = 8;
    destination.h  = 8;
    if(state->window == pico_null) {
        if(SDL_Init(SDL_INIT_VIDEO) < 0) {
            printf("SDL Error: %s\n", SDL_GetError());
            return;
        }
        state->window = SDL_CreateWindow("picogame",
                                         SDL_WINDOWPOS_UNDEFINED,
                                         SDL_WINDOWPOS_UNDEFINED,
                                         800,
                                         600,
                                         SDL_WINDOW_RESIZABLE | SDL_WINDOW_SHOWN);
        if(state->window == pico_null) {
            printf("SDL Error: %s\n", SDL_GetError());
            return;
        }
        state->renderer = SDL_CreateRenderer(state->window,
                                             -1,
                                             SDL_RENDERER_SOFTWARE);
        if(state->renderer == pico_null) {
            SDL_DestroyWindow(state->window);
            SDL_Quit();
            state->window = pico_null;
            printf("SDL Error: %s\n", SDL_GetError());
            return;
        }
        surface = SDL_LoadBMP("Grass.bmp");
        if(surface == pico_null) {
            SDL_DestroyRenderer(state->renderer);
            SDL_DestroyWindow(state->window);
            SDL_Quit();
            state->renderer = pico_null;
            state->window   = pico_null;
            printf("SDL Error: %s\n", SDL_GetError());
            return;
        }
        state->tileset = SDL_CreateTextureFromSurface(state->renderer, surface);
        if(state->tileset == pico_null) {
            SDL_FreeSurface(surface);
            SDL_DestroyRenderer(state->renderer);
            SDL_DestroyWindow(state->window);
            SDL_Quit();
            state->renderer = pico_null;
            state->window   = pico_null;
            printf("SDL Error: %s\n", SDL_GetError());
            return;
        }
        SDL_RenderSetLogicalSize(state->renderer, 128, 128);
        SDL_SetRenderDrawColor(state->renderer, 0, 0, 0, 255);
        state->old_time = 0;
        state->new_time = 0;
        state->delta_time = 0;
        state->destination = 1;
        start = SDL_GetTicks();
        frames = 0;
        state->type = 0;
    }
    state->new_time = SDL_GetTicks();
    state->delta_time = state->new_time - state->old_time;
    state->old_time = state->new_time;
    SDL_RenderClear(state->renderer);
    for(x = 0; x < 16; x++) {
        for(y = 0; y < 16; y++) {
            if(world->data[x][y] != 0) {
                source.x = world->data[x][y] * 8;
                destination.x = x * 8;
                destination.y = y * 8;
                SDL_RenderCopy(state->renderer, state->tileset, &source, &destination);
            }
        }
    }
    while(SDL_PollEvent(&event) != 0) {
        if(event.type == SDL_QUIT) {
            SDL_DestroyTexture(state->tileset);
            SDL_DestroyRenderer(state->renderer);
            SDL_DestroyWindow(state->window);
            state->renderer = pico_null;
            state->window   = pico_null;
            printf("%f\n", frames / ((SDL_GetTicks() - start) / 1000.0));
            SDL_Quit();
            message.type     = -1;
            message.receiver = self;
            pico_put(core, &message, 0, 0, pico_false);
            return;
        }
        if(event.type == SDL_MOUSEMOTION ||
           event.type == SDL_MOUSEBUTTONDOWN) {
            state->mouse_x = event.button.x;
            state->mouse_y = event.button.y;
            state->mouse_x >>= 3;
            state->mouse_y >>= 3;
            if(state->mouse_x < 16 &&
               state->mouse_y < 16) {
                if(event.button.button == SDL_BUTTON_LEFT) {
                    world->data[state->mouse_x][state->mouse_y] = state->type;
                }
                if(event.button.button == SDL_BUTTON_RIGHT) {
                    world->data[state->mouse_x][state->mouse_y] = 0;
                }
            }
        }
    }
    input = (f_message*)message.data;
    for(iterator = 1; iterator < pico_mailbox_count; iterator++) {
        if(core->mailboxes[iterator][0].free     == pico_false &&
           core->mailboxes[iterator][0].receiver == self) {
            switch(core->mailboxes[iterator][0].type) {
                case 1: {
                    object = (obj_message*)core->mailboxes[iterator][0].data;
                    if(object != pico_null) {
                        source.x      = object->type * 8;
                        destination.x = object->x * 8;
                        destination.y = object->y * 8;
                        SDL_RenderCopy(state->renderer, state->tileset, &source, &destination);
                    }
                    break;
                }
                default: break;
            }
            pico_take(core, pico_null, iterator, 0, pico_false);
        }
        if(pico_mailbox_size > 1) {
            input->delta_time = state->delta_time;
            message.receiver  = iterator;
            message.type      = 3;
            pico_put(core, &message, iterator, 1, pico_true);
        }
    }
    source.x = state->type * 8;
    destination.x = state->mouse_x * 8;
    destination.y = state->mouse_y * 8;
    SDL_RenderCopy(state->renderer, state->tileset, &source, &destination);
    SDL_RenderPresent(state->renderer);
    SDL_PumpEvents();
    keyboard = SDL_GetKeyboardState(pico_null);
    if(state->destination == 0                ||
       state->destination >= pico_actor_count ||
       state->destination >= pico_mailbox_count) {
        state->destination = 1;
    }
    message.type     = 2;
    if(keyboard[SDL_SCANCODE_LEFT]) {
        input->left = pico_true;
    }
    else {
        input->left = pico_false;
    }
    if(keyboard[SDL_SCANCODE_RIGHT]) {
        input->right = pico_true;
    }
    else {
        input->right = pico_false;
    }
    if(keyboard[SDL_SCANCODE_UP]) {
        input->up = pico_true;
    }
    else {
        input->up = pico_false;
    }
    if(keyboard[SDL_SCANCODE_DOWN]) {
        input->down = pico_true;
    }
    else {
        input->down = pico_false;
    }
    if(keyboard[SDL_SCANCODE_R]) {
        state->destination++;
    }
    if(keyboard[SDL_SCANCODE_F]) {
        state->destination--;
    }
    message.receiver = state->destination;
    pico_put(core, &message, state->destination, 0, pico_true);
    if(keyboard[SDL_SCANCODE_Q]) {
        message.type = 4;
        pico_put(core, &message, state->destination, 2, pico_false);
    }
    if(keyboard[SDL_SCANCODE_E]) {
        message.type = 5;
        pico_put(core, &message, state->destination, 2, pico_false);
    }
    if(keyboard[SDL_SCANCODE_1]) {
        state->type = 1;
    }
    if(keyboard[SDL_SCANCODE_2]) {
        state->type = 2;
    }
    if(keyboard[SDL_SCANCODE_3]) {
        state->type = 3;
    }
    if(keyboard[SDL_SCANCODE_4]) {
        state->type = 4;
    }
    if(keyboard[SDL_SCANCODE_5]) {
        state->type = 5;
    }
    if(keyboard[SDL_SCANCODE_6]) {
        state->type = 6;
    }
    if(keyboard[SDL_SCANCODE_7]) {
        state->type = 7;
    }
    if(keyboard[SDL_SCANCODE_8]) {
        state->type = 8;
    }
    if(keyboard[SDL_SCANCODE_9]) {
        state->type = 9;
    }
    if(keyboard[SDL_SCANCODE_0]) {
        state->type = 10;
    }
    frames++;
}

void object(pico_core  *core,
            pico_ucell  self) {
    c_state      *world;
    obj_state    *state;
    f_message    *input;
    obj_message  *output;
    pico_message  message;
    pico_ucell    iterator;
    world    = (c_state*)core->data;
    state    = (obj_state*)core->actors[self].data;
    if(self >= pico_mailbox_count) {
        return;
    }
    for(iterator = 0; iterator < pico_mailbox_size; iterator++) {
        if(core->mailboxes[self][iterator].free == pico_false &&
           core->mailboxes[self][iterator].type == 3) {
            input                   = (f_message*)core->mailboxes[self][iterator].data;
            state->delta_time       = input->delta_time;
            state->move_timer      += state->delta_time;
            state->animation_timer += state->delta_time;
        }
        if(core->mailboxes[self][iterator].free     == pico_false &&
           core->mailboxes[self][iterator].receiver == self) {
            switch(core->mailboxes[self][iterator].type) {
                case 2: {
                    input = (f_message*)core->mailboxes[self][iterator].data;
                    if(input         != pico_null &&
                       state->move_timer >= 50) {
                        if(input->left == pico_true) {
                            if(state->x                            != 0 &&
                               world->data[state->x - 1][state->y] == 0) {
                                state->x--;
                            }
                        }
                        if(input->right == pico_true) {
                            if(state->x                            != 15 &&
                               world->data[state->x + 1][state->y] == 0) {
                                state->x++;
                            }
                        }
                        if(input->up == pico_true) {
                            if(state->y                            != 0 &&
                               world->data[state->x][state->y - 1] == 0) {
                                state->y--;
                            }
                        }
                        if(input->down == pico_true) {
                            if(state->y                            != 15 &&
                               world->data[state->x][state->y + 1] == 0) {
                                state->y++;
                            }
                        }
                        state->move_timer = 0;
                    }
                    break;
                }
                case 4: {
                    if(state->move_timer >= 50) {
                        state->type--;
                        state->move_timer = 0;
                    }
                    break;
                }
                case 5: {
                    if(state->move_timer >= 50) {
                        state->type++;
                        state->move_timer = 0;
                    }
                    break;
                }
                default: break;
            }
            pico_take(core, pico_null, self, iterator, pico_false);
        }
    }
    if(state->animation_timer >= 256) {
        state->type = !state->type;
        state->animation_timer = 0;
    }
    output           = (obj_message*)message.data;
    output->x        = state->x;
    output->y        = state->y;
    output->type     = state->type;
    message.sender   = self;
    message.receiver = 0;
    message.type     = 1;
    pico_put(core, &message, self, 0, pico_true);
    return;
}

int main(void) {
    static pico_core  core;
           pico_ucell iterator;
    printf("%lu\n", sizeof(pico_actor));
    printf("%lu\n", sizeof(pico_core));
    if(pico_core_new(&core, update) == pico_false) {
        return -1;
    }
    srand(time(pico_null));
    pico_actor_new(&core, frontend, 0, 0);
    for(iterator = 1; iterator < pico_core_actor_count; iterator++) {
        pico_actor_new(&core, object, 0, iterator);
    }
    pico_core_run(&core);
    return 0;
}
