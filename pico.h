#if !defined(pico_included)
#if !defined(pico_packet_data_size)
    #define pico_packet_data_size 256
#endif
#if !defined(pico_port_packet_count)
    #define pico_port_packet_count 256
#endif
#if !defined(pico_core_port_count)
    #define pico_core_port_count 256
#endif
#if !defined(pico_actor_data_size)
    #define pico_actor_data_size 256
#endif
#if !defined(pico_core_actor_count)
    #define pico_core_actor_count 256
#endif
#if !defined(pico_core_data_size)
    #define pico_core_data_size 256
#endif
#define pico_null ((void*)0)
typedef signed        pico_cell;
typedef unsigned      pico_ucell;
typedef unsigned char pico_byte;
typedef unsigned char pico_bool;
enum {
    pico_false = 0,
    pico_true  = !pico_false
};

typedef struct pico_packet pico_packet;
typedef struct pico_port   pico_port;
typedef struct pico_actor  pico_actor;
typedef struct pico_core   pico_core;

struct pico_packet {
    pico_byte  data[pico_packet_data_size];
    pico_ucell owner;
    pico_ucell type;
};

struct pico_actor {
    pico_byte    data   [pico_actor_data_size];
    void       (*update)(pico_core*,
                         pico_ucell);
    pico_ucell   type;
};

struct pico_core {
    pico_packet   ports  [pico_core_port_count][pico_port_packet_count];
    pico_actor    actors [pico_core_actor_count];
    pico_byte     data   [pico_core_data_size];
    pico_bool   (*update)(pico_core*);
};

extern pico_ucell pico_packet_size;
extern pico_ucell pico_actor_size;
extern pico_ucell pico_actor_count;
extern pico_ucell pico_port_size;
extern pico_ucell pico_port_count;
extern pico_ucell pico_core_size;

pico_bool pico_init        (pico_ucell   actor_size,
                            pico_ucell   actor_count,
                            pico_ucell   port_size,
                            pico_ucell   port_count,
                            pico_ucell   core_size);
pico_bool pico_data_set    (pico_byte   *data,
                            pico_ucell   size,
                            pico_byte    value);
pico_bool pico_port_clear  (pico_core   *core,
                            pico_ucell   port);
pico_bool pico_actor_new   (pico_core   *core,
                            void       (*update)(pico_core*,
                                                 pico_ucell),
                            pico_ucell   type,
                            pico_ucell   id);
pico_bool pico_actor_delete(pico_core   *core,
                            pico_ucell   id);
pico_bool pico_core_new    (pico_core   *core,
                            pico_bool  (*update)(pico_core*));
pico_bool pico_core_run    (pico_core   *core);
#endif
