#include <pico.h>

pico_ucell pico_packet_size = pico_packet_data_size;
pico_ucell pico_actor_size  = pico_actor_data_size;
pico_ucell pico_actor_count = pico_core_actor_count;
pico_ucell pico_port_size   = pico_port_packet_count;
pico_ucell pico_port_count  = pico_core_port_count;
pico_ucell pico_core_size   = pico_core_data_size;

pico_bool pico_init(pico_ucell actor_size,
                    pico_ucell actor_count,
                    pico_ucell port_size,
                    pico_ucell port_count,
                    pico_ucell core_size) {
    if(actor_size  != pico_actor_size  ||
       actor_count != pico_actor_count ||
       port_size   != pico_port_size   ||
       port_count  != pico_port_count  ||
       core_size   != pico_core_size) {
        return pico_false;
    }
    return pico_true;
}

pico_bool pico_data_set(pico_byte  *data,
                        pico_ucell  size,
                        pico_byte   value) {
    pico_ucell iterator;
    if(data == pico_null) {
        return pico_false;
    }
    for(iterator = 0; iterator < size; iterator++) {
        data[iterator] = value;
    }
    return pico_true;
}

pico_bool pico_port_clear(pico_core  *core,
                          pico_ucell  port) {
    pico_byte  *data;
    pico_ucell  iterator;
    if(core == pico_null ||
       port >= pico_port_count) {
        return pico_false;
    }
    for(iterator = 0; iterator < pico_port_size; iterator++) {
        data = (pico_byte*)core->ports[port][iterator].data;
        pico_data_set(data, pico_packet_size, 0);
        core->ports[port][iterator].owner = pico_actor_count;
        core->ports[port][iterator].type  = 0;
    }
    return pico_true;
}

pico_bool pico_actor_new(pico_core   *core,
                         void       (*update)(pico_core*,
                                              pico_ucell),
                         pico_ucell   type,
                         pico_ucell   id) {
    if(core                    == pico_null        ||
       id                      >= pico_actor_count ||
       core->actors[id].update != pico_null) {
        return pico_false;
    }
    core->actors[id].update = update;
    core->actors[id].type   = type;
    return pico_true;
}

pico_bool pico_actor_delete(pico_core  *core,
                            pico_ucell  id) {
    pico_byte *data;
    if(core == pico_null ||
       id   >= pico_actor_count) {
        return pico_false;
    }
    data = (pico_byte*)core->actors[id].data;
    pico_data_set(data, pico_actor_size, 0);
    core->actors[id].update = pico_null;
    core->actors[id].type   = 0;
    return pico_true;
}

pico_bool pico_core_new(pico_core  *core,
                        pico_bool (*update)(pico_core*)) {
    pico_byte  *data;
    pico_ucell  iterator;
    if(core == pico_null) {
        return pico_false;
    }
    for(iterator = 0; iterator < pico_port_count; iterator++) {
        pico_port_clear(core, iterator);
    }
    for(iterator = 0; iterator < pico_actor_count; iterator++) {
        pico_actor_delete(core, iterator);
    }
    data = (pico_byte*)core->data;
    pico_data_set(data, pico_core_size, 0);
    core->update = update;
    return pico_true;
}

pico_bool pico_core_run(pico_core *core) {
    if(core         == pico_null ||
       core->update == pico_null) {
        return pico_false;
    }
    while(core->update(core) == pico_true) {
        continue;
    }
    return pico_true;
}
